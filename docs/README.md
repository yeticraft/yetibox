Please see the [wiki](https://gitlab.com/yeticraft/yetibox/wikis/) for more info.

This directory is mainly a dumping ground for bash histories, thoughts and random notes between the project admins.  Everything in here is likely to be very temporary.