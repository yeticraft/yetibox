# YetiBox

This is a placeholder that @Footpad will replace, shortly.

Until he DOES edit it - the longer goal description of this project is to generate full tutorials and reference documentation for multiple small ro medium sized projects that can collectively be used to create a fully functioning network of devices, services, and applications for personal, home, and office needs.

There will also be some code snippets and examples here, but the main source libraries for individual projects will be within those linked projects.